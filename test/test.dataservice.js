var expect = chai.expect;

describe('dataService', function() {

	var dataService;
	var $rootScope;

	beforeEach(function() {
		
		module('walmart-test');

		inject(function(_dataService_, _$rootScope_){
			dataService = _dataService_;
			$rootScope_ = _$rootScope_;
		});

	});	

	describe('dataService.store', function() {
		it('should store and retrieve an object', function() {
			
			var testObject = {
				address: 'rua do teste',
				country: 'Brasil'
			};

			dataService.store('shippingAddress', testObject, true);

			var testResult = dataService.retrieve('shippingAddress');

			expect(testResult).to.be.a('object');
			
			expect(testResult).to.have.all.keys(['address', 'country']);

		});

		it('should store and retrieve an unique object', function() {
			
			var testObject = {
				address: 'rua do teste',
				country: 'Brasil'
			};

			dataService.store('shippingAddress', testObject, false);

			var testResult = dataService.retrieve('shippingAddress');

			expect(testResult).to.be.a('array');
			
			expect(testResult[0]).to.have.all.keys(['address', 'country']);

		});

		it('should store multiple objects with the same key', function() {
			
			var testObject1 = {
				address: 'rua do teste',
				country: 'Brasil'
			};

			var testObject2 = {
				address: 'rua do teste 2',
				country: 'Brasil'
			}

			dataService.store('shippingAddress', testObject1, false);
			dataService.store('shippingAddress', testObject2, false);

			var testResult = dataService.retrieve('shippingAddress');

			expect(testResult).to.be.a('array');
			
			expect(testResult.length).to.equal(2);
			expect(testResult[0]).to.have.all.keys(['address', 'country']);
			expect(testResult[1]).to.have.all.keys(['address', 'country']);

		});
	});
});
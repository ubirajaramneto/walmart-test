/*
* This script is responsible for transforming the navigation menu into
* a responsive menu.
*/

(function () {

	// Declare all variables and assign them the DOM elements
	var toogle_button = document.getElementById('toggle-menu');
	var menu = document.getElementById('menu');
	var menuItens = document.querySelectorAll('li>a.pure-menu-link');

	// Attach event listener for menu button click event
	toogle_button.addEventListener('click', open_menu, false);

	// Animate the navigation
	function open_menu() {

		// This class will increase the height of the menu nav
		menu.classList.toggle('active-menu');

		for (var i = menuItens.length - 1; i >= 0; i--) {
			
			// This class will add the display:none css property
			// for every menu item.
			menuItens[i].classList.toggle('inactive-menu-item');
		}
	}

})();
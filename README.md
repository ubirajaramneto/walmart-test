# README

Esse projeto será feito como avaliação para o processo seletivo do walmart, detalhes do projeto serão mantidos fora deste documento por questão de segurança.

## STACK

Devido as restrições de tempo e pesquisa para o projeto, tudo será feito com bibliotecas bem conhecidas e será enxugado ao minimo para melhorar a legibilidade e entendimento do projeto.

Os frameworks e ferramentas utilizados serão:

* Angular.js 1.5.3 - Framework MVC

* Angular UI Router	- Roteador para Angular.js

* Mocha - framework de teste

* Chai - Biblioteca de asserção

* Grunt - Automação

* Bower - Gerenciador de dependencias para front-end

* PureCSS - Framework CSS Minimalista

* HTML5

* CSS3

## GUIA

https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md

Para esse projeto será utilizado o guia de estilo especificado nesse link para todo código que for escrito em Angular, e deverá seguir todas as decisões de design sobre como o aplicativo deve se comportar.

## DOCUMENTAÇÃO

A documentação interna do código será gerada de forma generica com o plugin JSDOC.

O objetivo dessa documentação é facilitar a visualização da estrutura do aplicativo
como um todo e ver quais são os pontos chave da arquitetura em si.

OBS: o plugin não foi feito para documentar aplicações Angular.js, então a documentação
não será 100% fiel ao estilo de codificação e estrutura de uma aplicação Angular.js comum.

## TESTES

Para rodar os testes, abra a página index.html que está localizada no diretório 'test', abra o inspetor do navegador, na aba console, para ver todos os testes executarem.

## DESIGN

O objetivo desta aplicação é demonstrar como pude construir uma plataforma utilizando frameworks bem conhecidos.

Alguns dos outros objetivos são:

* Demonstrar capacidade de montar casos de testes unitários

* Demonstrar habilidades de codificação e design de estruturas de dados simples

* Demonstrar boas práticas de UX aplicando recursos para navegação responsiva

* Demonstrar boas práticas de documentação e automação de tarefas comuns em um projeto de front-end
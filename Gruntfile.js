module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: ['src/app.js', 'src/**/*.js'],
        dest: 'build/app.min.js'
      },
      assets_build: {
        src: 'assets/js/responsive.js',
        dest: 'assets/dist/ui.js'
      },
      polyfills_build: {
        src: 'assets/js/polyfills.js',
        dest: 'assets/dist/polyfills.min.js'
      }
    },
    jsdoc : {
      dist : {
        src: ['src/*.js', 'src/**/*.js'],
        options: {
            destination: 'doc'
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  //Load Js documentation generation plugin
  grunt.loadNpmTasks('grunt-jsdoc');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'jsdoc']);

};
/**
	*@namespace ShippingList
	*@description This is the directive for the shipping list
	*/

(function() {

	'use strict';

	angular
		.module('walmart-test')
		.directive('wlmtShippingList', wlmtShippingList);

		/**
			*@name wlmt-shipping-list
			*@memberOf ShippingList
			*@access private
			*@example <wlmt-shipping-list></wlmt-shipping-list>
			*/
		function wlmtShippingList() {
			var directive = {
				restrict: 'EA',
				templateUrl: 'src/shipping/directives/shipping_list.html',
				controller: ShippingListController,
				controllerAs: 'shippingListVm',
				bindToController: true
			}
			return directive;
		}

		ShippingListController.$inject = ['dataService', '$rootScope', '$scope'];

		/**
			*@name ShippingListController
			*@memberOf ShippingList
			*@property {object} shippingListVm - This binding for the scope model
			*@property {array} shipping_addresses - Model for holding shipping address array
			*/
		function ShippingListController(dataService, $rootScope, $scope) {

			var shippingListVm = this;

			$rootScope.$watch('data', function(current, original) {
				shippingListVm.shipping_addresses = dataService.retrieve('shippingAddress');
			});
			

		}

})();
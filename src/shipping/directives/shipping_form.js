/**
	*@namespace ShippingForm
	*@description This is the directive for the shipping form
	*/

(function() {

	'use strict';

	angular
		.module('walmart-test')
		.directive('wlmtShippingForm', wlmtShippingForm);

	/**
		*@name wlmt-shipping-form
		*@memberOf ShippingForm
		*@access private
		*@example <wlmt-shipping-form></wlmt-shipping-form>
		*/
	function wlmtShippingForm() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'src/shipping/directives/shipping_form.html',
			controller: ShippingFormController,
			controllerAs: 'shippingFormVm',
			bindToController: true
		}
		return directive;
	}

	ShippingFormController.$inject = ['dataService'];

	/**
		* @name ShippingFormController
		* @memberOf ShippingForm
		* @access private
		* @property {object} shippingFormVm - This binding to scope model
		*/
	function ShippingFormController(dataService) {

		var shippingFormVm = this;

		/**
			* @name uiState
			* @memberOf ShippingForm
			*	@property {boolean} shippingFormVm.uiState.form - Form visibility state
			*/
		// This will hold the state for the ui of the directive
		shippingFormVm.uiState = {
			form: false
		};

		/**
			* @name uiControl
			* @memberOf ShippingForm
			*	@property {function} shippingFormVm.uiState.toggleForm - Form ui control actions
			*/
		// This will hold the functions that will manipulate the directive
		shippingFormVm.uiControl = {
			toggleForm: toggleForm
		}

		/**
			* @name uiMessages
			* @memberOf ShippingForm
			*	@property {boolean} shippingFormVm.uiMessages.formSubmitionPristine 
			*	- This will tell if the form is pristine (not yet interacted with) or not.
			* @property {boolean} shippingFormVm.uiMessages.formSubmitionSucceded
			* - This will tell if the submition of the form was successfull or not.
			* @property {string} shippingFormVm.uiMessages.formSubmitionMsg
			* - This will hold the message for the form submission status.
			*/
		// This will hold the operations messages for the form submition process
		shippingFormVm.uiMessages = {
			formSubmitionPristine: true,
			formSubmitionSucceded: false,
			formSubmitionMsg: ""
		}

		/**
			* @name actions
			* @memberOf ShippingForm
			* @property {function} shippingFormVm.actions.submitForm - This is the
			* form submition action.
			*/
		// This will hold all possible actions for this controller
		shippingFormVm.actions = {
			submitForm: submitForm
		}

		// Toggle the form on or off and reset form submission result messages
		function toggleForm() {
			// this is just a smart way to toggle a variable true or false ;)
			shippingFormVm.uiState.form = !shippingFormVm.uiState.form;
			resetMsgs();
		}

		/* 
			*This will submit the form and store the value in the local persistency layer
			* It will also set the messages indicating the success of the operation
			*/
		function submitForm() {
			var _shipping_address = new Shipping(shippingFormVm.address, shippingFormVm.city);
			var _form_submition_result = dataService.store('shippingAddress', _shipping_address, false, true);
			handleSubmit();
			toggleForm();
		}

		/*
		* sorry if this got a little messy but since the whole file is not too large
		* these kind of state transitions in the UI should be easy to maintain, if
		* they got too big then it would be necessary to transition this kind of
		* responsability to a ui manager of some sort that could listen to http
		* requests and show the proper error msg from the backend.
		*/

		// This function will reset the ui messages in this controller
		function resetMsgs() {
			if(shippingFormVm.uiState.form === true) {
				shippingFormVm.uiMessages.formSubmitionPristine = true;
				shippingFormVm.uiMessages.formSubmitionSucceded = false;
				shippingFormVm.uiMessages.formSubmitionMsg = '';
			}			
		}

		// This function will handle successfull events and process the ui changes
		function handleSubmit() {
			shippingFormVm.uiMessages.formSubmitionPristine = false;
			shippingFormVm.uiMessages.formSubmitionSucceded = true;
			shippingFormVm.uiMessages.formSubmitionMsg = "Endereço salvo com sucesso!";
		}

	}

	/**
		*@class
		*@memberOf ShippingForm
		*@access private
		*@property {string} address
		*@property {string} city
		*/
	function Shipping(address, city) {
		this.address = address;
		this.city = city;
	}

})();
(function() {

	'use strict';

	angular.module('walmart-test', ['ui.router'])

	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

		// If no route is matched, then use the one below
		$urlRouterProvider.otherwise('/user');

		//===================== STATES ===========================
		$stateProvider
		.state('userDetails', {
			url: '/user',
			templateUrl: 'src/user/templates/user.html'
		})
		.state('orderDetails', {
			url: '/orders',
			templateUrl: 'src/orders/templates/orders.html'
		});
		//==================END OF STATES DEFINITION==============

	}]);

})();

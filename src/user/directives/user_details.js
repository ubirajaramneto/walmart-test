/**
	* @namespace User
  * @description This is the angular namespace for the user details directive.
  */
(function() {

	'use strict';
	
	angular
		.module('walmart-test')
		.directive('wlmtUserDetails', wlmtUserDetails);

	/**
		* @name wlmt-user-details
		* @memberOf User
		* @access public
		* @example <wlmt-user-details></wlmt-user-details>
		*/

	// Definition of the directive object to return
	function wlmtUserDetails() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'src/user/directives/user_details.html',
			controller: UserController,
			controllerAs: 'userDetailsVm',
			bindToController: true
		}

		return directive;
	}

	 /**
		* @name UserController
		* @memberOf User
		* @access private
		* @property {object} userDetailsVm - This binding to scope model
		*/
	UserController.$inject = ['dataService'];

	function UserController(dataService) {
		var userDetailsVm = this;
		userDetailsVm.user = dataService.getUserData();
	}

})();
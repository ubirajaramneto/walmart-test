/**
	*@namespace OrderDetails
	*@description This is the directive for the oder details page
	*/

(function() {

	'use_strict';

	angular
	.module('walmart-test')
	.directive('wlmtOrderDetails', wlmtOrderDetails);

	/**
		*@name wlmt-order-details
		*@memberOf OrderDetails
		*@access private
		*@example <wlmt-order-details></wlmt-order-details>
		*/	
	function wlmtOrderDetails() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'src/orders/directives/order_details.html',
			controller: OrderDetailsController,
			controllerAs: 'orderDetailsVm',
			bindToController: true
		}

		return directive;
	}

	/**
		*@name OrderDetailsController
		*@memberOf OrderDetails
		*@property {object} orderDetailsVm - This binding for the scope model
		*@property {string} product_name - Name of the product
		*@property {string} product_price - Price of the product
		*@property {string} product_photo - Image url for the product
		*@property {string} product_status - The status of the order
		*/
	function OrderDetailsController() {

		var orderDetailsVm = this;

		orderDetailsVm.order = {
			product_name: 'Iphone 6',
			product_price: 'U$ 600,00',
			product_photo: 'assets/images/iphone.jpg',
			product_status: 'Despachado'
		};

	}

})();